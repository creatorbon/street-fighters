import LeftKick from '../../resources/text/kick-left.png';
import LeftBlock from '../../resources/text/block-left.png';
import LeftCombo from '../../resources/text/combo-left.png';
import RightKick from '../../resources/text/kick-right.png';
import RightBlock from '../../resources/text/block-right.png';
import RightCombo from '../../resources/text/combo-right.png';

export const fighterText = {
    LeftKick,
    LeftBlock,
    LeftCombo,
    RightKick,
    RightBlock,
    RightCombo,
};  