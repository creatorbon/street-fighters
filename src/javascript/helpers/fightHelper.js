export function calcHealth(totalHealth, currentHealth) {
    let health = 0;
    health = (currentHealth / totalHealth) * 100;
    return health < 0 ? `${0}%` : `${health}%`;
}

export function getRandom(min, max) {
    return Math.random() * (max - min) + min;
}