import { controls } from '../../constants/controls';
import { getRandom, calcHealth } from '../helpers/fightHelper';
import { fighterText } from '../../constants/fighterText';

const { PlayerOneCriticalHitCombination } = controls;
const [PlayerOne1Button, PlayerOne2Button, PlayerOne3Button] = PlayerOneCriticalHitCombination;
const { PlayerTwoCriticalHitCombination } = controls;
const [PlayerTwo1Button, PlayerTwo2Button, PlayerTwo3Button] = PlayerTwoCriticalHitCombination;

export async function fight(firstFighter, secondFighter) {
  let firstFighterHeals = document.getElementById('left-fighter-indicator');
  let secondFighterHeals = document.getElementById('right-fighter-indicator');
  let firstFighterComboStatus = document.getElementById('left-fighter-combo-status');
  let secondFighterComboStatus = document.getElementById('right-fighter-combo-status');
  let firstFighterText = document.getElementById('left-fighter-text');
  let secondFighterText = document.getElementById('right-fighter-text');
  let firstFighterLockedCombo = false;
  let secondFighterLockedCombo = false;

  return new Promise((resolve) => {
    let pressedKeys = [];
    let playerOneCurrentHealth = firstFighter.health;
    let playerTwoCurrentHealth = secondFighter.health;
    // resolve the promise with the winner when fight is over
    document.addEventListener('keydown', function (event) {
      pressedKeys[event.code] = true;
      if (!pressedKeys[controls.PlayerOneBlock]) { // check if first player block
        if (event.code === controls.PlayerOneAttack && !event.repeat) { // check if player hold attack button
          if (pressedKeys[controls.PlayerTwoBlock]) { // when second fighter hold block
            secondFighterText.src = fighterText.RightBlock;
          } else {
            firstFighterText.src = fighterText.LeftKick;
            playerTwoCurrentHealth -= getDamage(firstFighter, secondFighter);
            secondFighterHeals.style.width = calcHealth(secondFighter.health, playerTwoCurrentHealth);
          }
        }
        if (pressedKeys[PlayerOne1Button] && pressedKeys[PlayerOne2Button] && pressedKeys[PlayerOne3Button]) { 
          if (firstFighterLockedCombo) return; // check if player can use combo

          firstFighterLockedCombo = true;
          firstFighterComboStatus.style.visibility = 'hidden';
          firstFighterText.src = fighterText.LeftCombo;
          playerTwoCurrentHealth -= critHit(firstFighter);
          secondFighterHeals.style.width = calcHealth(secondFighter.health, playerTwoCurrentHealth);
          setTimeout(() => { firstFighterLockedCombo = false; firstFighterComboStatus.style.visibility = 'visible'; }, 10000);
        }
      }

      if (!pressedKeys[controls.PlayerTwoBlock]) {
        if (event.code === controls.PlayerTwoAttack && !event.repeat) {
          if (pressedKeys[controls.PlayerOneBlock]) {
            firstFighterText.src = fighterText.LeftBlock;
          } else {
            secondFighterText.src = fighterText.RightKick;
            playerOneCurrentHealth -= getDamage(secondFighter, firstFighter);
            firstFighterHeals.style.width = calcHealth(firstFighter.health, playerOneCurrentHealth);
          }
        }
        if (pressedKeys[PlayerTwo1Button] && pressedKeys[PlayerTwo2Button] && pressedKeys[PlayerTwo3Button]) {
          if (secondFighterLockedCombo) return;

          secondFighterLockedCombo = true;
          secondFighterComboStatus.style.visibility = 'hidden';
          secondFighterText.src = fighterText.RightCombo;
          playerOneCurrentHealth -= critHit(secondFighter);
          firstFighterHeals.style.width = calcHealth(firstFighter.health, playerOneCurrentHealth);
          setTimeout(() => { secondFighterLockedCombo = false; secondFighterComboStatus.style.visibility = 'visible'; }, 10000);
        }
      }

      if (playerOneCurrentHealth < 0) return resolve(secondFighter);
      if (playerTwoCurrentHealth < 0) return resolve(firstFighter);
    });

    document.addEventListener('keyup', function (event) {
      pressedKeys[event.code] = false;
    });
  })
}

export function getDamage(attacker, defender) {
  // return damage
  let damage = getHitPower(attacker) - getBlockPower(defender);
  return damage < 0 ? 0 : damage;
}

export function getHitPower(fighter) {
  // return hit power
  let criticalHitChance = getRandom(1, 2);
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block power
  let dodgeChance = getRandom(1, 2);
  return fighter.defense * dodgeChance;
}

function critHit(fighter) {
  return fighter.attack * 2;
}